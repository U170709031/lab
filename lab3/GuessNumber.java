

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		//System.out.println(number);  sayıyı gösteriyor bitirirken sil bu satırı
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        int attempt = 1;
        while (true){
		System.out.print("Can you guess it: ");

		
		int guess = reader.nextInt(); //Read the user input
        

        if (guess == number)
{
    System.out.println("Congratulations!");
    System.out.println("You won after "+attempt+" attempts");
break;
}
        else if(guess == -1)
{
        System.out.println("Sorry, the number was "+number);
        System.out.println("You lose after "+(attempt - 1)+" attempts");
break;
}
        else
{
    System.out.println("Sorry!");
    if (guess > number)
{
    System.out.println("True number is lesser than your guess");
}
    else if(guess < number)
{
    System.out.println("True number is higher than your guess");
}
    
}
    attempt = attempt + 1;
		
		}
		reader.close(); //Close the resource before exiting
	}
	
    
	
}
