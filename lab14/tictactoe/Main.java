package tictactoe;

import java.util.*;

public class Main {

	public static void main(String[] args)  {
		Scanner reader = new Scanner(System.in);

		Board board = new Board();
		
		System.out.println(board);
		while (!board.isEnded()) {
			
			int player = board.getCurrentPlayer();
			boolean invalidRow = false;
			boolean invalidCol = false;
			int row = 0;
			int col = 0;
			do {
				System.out.print("Player "+ player + " enter row number:");
			
				try {
					row = reader.nextInt();
					invalidRow = false;
				}
				
				catch(InputMismatchException ex) {
						System.out.println("Invalid row");
						reader.nextLine();
						invalidRow = true;
					}
				
				} while(invalidRow);
			
			do {
				System.out.print("Player "+ player + " enter column number:");
				try {
					col = reader.nextInt();
					invalidCol = false;
				}
				
				catch(InputMismatchException ex) {
					System.out.println("Invalid column");
					reader.nextLine();
					invalidCol = true;
				}
			} while(invalidCol);
			
			try {
			board.move(row, col);}
			catch(Exception ex) {
				System.out.println(ex.getMessage());
			}
			System.out.println(board);
		}
		
		
		reader.close();
	}



}
