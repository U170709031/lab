import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);
        int moveCount = 0;
    while (moveCount < 9)
    {	int row;
    	int col;
    	while (true)
    	{
    		while (true)
    		{ 
    		System.out.print("Player 1 enter row number:");
    	 	row = reader.nextInt();
		 	if (row <= 3 && row > 0)
			 break;
		 	else
		 		System.out.println("Invalid value");
		 
    		}   
         
    		while (true) 
    		{
			 System.out.print("Player 1 enter column number:");
			 col = reader.nextInt();
			 if (col <= 3 && col > 0)
				 break;
			 else
				 System.out.println("Invalid value");
    		}
        
    		if (board[row -1][col -1] == ' ' )
    			{
    				board[row - 1][col - 1] = 'X';
    				break;
    			}
    		else
    			System.out.println("Error");
    		}
    		printBoard(board);
    		boolean kazandiX = false;
        
        for (int i = 0; i < 3; i++)  //yatayDeneme x
        {   
            for (int j = 0; j < 3; j++)
            {
                if (board[i][j] != 'X')
                    break;
                else if (board[i][j] == 'X' && j == 2)
                    kazandiX = true;                                
            }
        }

        for (int i = 0,j = 0; i < 3; i++,j++)
        {
            if (board[i][j] != 'X')
                    break;
                else if (board[i][j] == 'X' && j == 2)
                    kazandiX = true;
                    
        }

        for (int i = 0, j = 2; i < 3; i++, j--)
        {
            if (board[i][j] != 'X')
                    break;
                else if (board[i][j] == 'X' && i == 2)
                    kazandiX = true;
                    
        }
        
                
       

        for (int i = 0; i < 3; i++)  //dikeyDeneme x
        {   
            for (int j = 0; j < 3; j++)
            {
                if (board[j][i] != 'X')
                    break;
                else if (board[j][i] == 'X' && j == 2)
                    kazandiX = true;
                
            }
        }

        
        if (kazandiX)
        {
        	System.out.println("Player 1 (X) kazandı");
            break;
        }    
        while (true)
    	{
    		while (true)
    		{ 
    		System.out.print("Player 2 enter row number:");
    	 	row = reader.nextInt();
		 	if (row <= 3 && row > 0)
			 break;
		 	else
		 		System.out.println("Invalid value");
		 
    		}   
         
    		while (true) 
    		{
			 System.out.print("Player 2 enter column number:");
			 col = reader.nextInt();
			 if (col <= 3 && col > 0)
				 break;
			 else 
				 System.out.println("Invalid value");
    		}
        
    		if (board[row -1][col -1] == ' ' )
    			{
    				board[row - 1][col - 1] = 'O';
    				break;
    			}
    		else
    			System.out.println("Error");
    		}
		
		for (int i = 0, j = 2; i < 3; i++, j--)
        {
            if (board[i][j] != 'O')
                    break;
                else if (board[i][j] == 'X' && i == 2)
                    kazandiX = true;
                    
        }
        
		printBoard(board);
        boolean kazandiO = false;
        for (int i = 0; i < 3; i++)    //yatay denemeO
        {   
            for (int j = 0; j < 3; j++)
            {
                if (board[i][j] != 'O')
                    break;
                else if (board[i][j] == 'O' && j == 2)
                    kazandiO = true;
                    
                
            }
        }
    
        for (int i = 0,j = 0; i < 3; i++,j++)
        {
            if (board[i][j] != 'O')
                    break;
                else if (board[i][j] == 'O' && j == 2)
                    kazandiO = true;
                    
        }
        
        for (int i = 0, j = 2; i < 3; i++, j--)
        {
            if (board[i][j] != 'O')
                    break;
                else if (board[i][j] == 'O' && i == 2)
                    kazandiO = true;
                    
        }
        


        for (int i = 0; i < 3; i++)  //dikeyDeneme x
        {   
            for (int j = 0; j < 3; j++)
            {
                if (board[j][i] != 'O')
                    break;
                else if (board[j][i] == 'O' && j == 2)
                    kazandiO = true;
                    
            }
        }

        if (kazandiO)
        	{
        		System.out.println("Player 2 (O) kazandı");
        		break;
        	}
        moveCount+=1;
    }
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
